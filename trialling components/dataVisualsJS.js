const pollData = [
  {
    option: "Physics",
    votes: 11,
    color: "rgb(255, 99, 132)"
  },
  {
    option: "English",
    votes: 8,
    color: "rgb(54, 162, 235)"
  },
  {
    option: "Maths",
    votes: 11,
    color: "rgb(36, 36, 36)"
  },
  {
    option: "Business",
    votes: 5,
    color: "rgb(255, 159, 64)"
  },

  {
    option: "Economics",
    votes: 3,
    color: "rgb(75, 192, 192)"
  },
  {
    option: "DT",
    votes: 8,
    color: "rgb(255, 206, 86)"
  },
  {
    option: "RE",
    votes: 10,
    color: "rgb(153, 102, 255)"
  },
];
const ctx = document.getElementById('chart').getContext('2d');
const pollChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: pollData.map(pollOption => pollOption.option),
        datasets: [{

            // label: '# of Credits',
            data: pollData.map(pollOption => pollOption.votes),
            options: {
            legend: {
              display: false
            }
          },
            backgroundColor: pollData.map(pollOption => pollOption.color),
            borderWidth: 1
        }]

    // options: {
    //     scales: {
    //         yAxes: [{
    //           ticks: {
    //             beginAtZero: true
    //         }
    //     }]
    // },
    // title: {
    //   display: true,
    //   text: 'Subject Credits',
    //   fontColor: "#333",
    //   fontSize: 20,
    //   padding: 20
    // },
    // legend: {
    //   display: false,
    // }
}
});

// const pollData = [
//   {
//     option: "Reading",
//     votes: 11,
//     color: "rgb(255, 99, 132)"
//   },
//   {
//     option: "Writing",
//     votes: 8,
//     color: "rgb(54, 162, 235)"
//   },
//   {
//     option: "Literacy",
//     votes: 11,
//     color: "rgb(36, 36, 36)"
//   },
//
// ];
// const ctx = document.getElementById('chart2').getContext('2d');
// const pollChart = new Chart(chart2, {
//     type: 'pie',
//     data: {
//         labels: pollData.map(pollOption => pollOption.option),
//         datasets: [{
//
//             // label: '# of Credits',
//             data: pollData.map(pollOption => pollOption.votes),
//             options: {
//             legend: {
//               display: false
//             }
//           },
//             backgroundColor: pollData.map(pollOption => pollOption.color),
//             borderWidth: 1
//         }]
//
//     // options: {
//     //     scales: {
//     //         yAxes: [{
//     //           ticks: {
//     //             beginAtZero: true
//     //         }
//     //     }]
//     // },
//     // title: {
//     //   display: true,
//     //   text: 'Subject Credits',
//     //   fontColor: "#333",
//     //   fontSize: 20,
//     //   padding: 20
//     // },
//     // legend: {
//     //   display: false,
//     // }
// }
// });
