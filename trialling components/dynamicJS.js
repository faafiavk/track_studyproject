//Credits Chart
// Term 1
var term1data = [0, 10, 5, 2, 20, 30, 45];
var term1data = [10, 20, 30, 42, 50, 60, 70];

// Term 2
var term2data = [99, 80, 75, 62, 50, 40, 35];
var term2data = [5, 10, 55, 15, 20, 55, 25];

// Term 3
var term3data = [99, 80, 75, 62, 50, 40, 35];
var term3data = [5, 10, 55, 15, 20, 55, 25];

// Term 4
var term4data = [99, 80, 75, 62, 50, 40, 35];
var term4data = [5, 10, 55, 15, 20, 55, 25];

var ctx = document.getElementById('creditsChart').getContext('2d');
var chart = new Chart(ctx, {

//Type of chart
    type: 'line',

//The data for the dataset
    data: {
        labels: ['Physics', 'English', 'Economics', 'Business', 'Maths', 'DT'],
        datasets: [{

          // Term 1 Dataset
            label: 'My First Dataset',
            //backgroundColor: 'rgb(0, 99, 132)',
            borderColor: 'rgb(0, 99, 132)',
            data: term1data,
          },

        // Term 2 Dataset
          {
            label: 'My Second Dataset',
            //backgroundColor: 'rgb(0, 200, 132)',
            borderColor: 'rgb(0, 200, 132)',
            data: term2data,
          }
        ]
      },

        // Term 3 Dataset
          {
            label: 'My Third Dataset',
            //backgroundColor: 'rgb(0, 200, 132)',
            borderColor: 'rgb(0, 200, 132)',
            data: term3data,
          }
        ]
      },

        // Term 4 Dataset
          {
            label: 'My Fourth Dataset',
            //backgroundColor: 'rgb(0, 200, 132)',
            borderColor: 'rgb(0, 200, 132)',
            data: term4data,
          }
        ]
        },
//Configuration options
    options: {}
});

// This updates the chart and is the function for the update button
  function updateChart() {
    chart.data.datasets[0].data = term1data;
    chart.data.datasets[1].data = term2data;
    // chart.data.labels = ["August", "September", "October", "November", "December"];
    chart.update();
  };


//pop = remove last value
//shift = remove first value
//unshift = add the value at the very beginning of the array
//push = add the value at the very end of the array

// This adds a value to the y axis
//   function addValue() {
//     chart.data.datasets[0].data.reverse();
//     //chart.data.datasets[0].data.push(999);
//     chart.data.labels.push("January");
//     chart.update();
//   };

// UE Chart
// var ctx = document.getElementById('UEChart').getContext('2d');
// var myChart = new Chart(ctx, {
//     type: 'bar',
//     data: {
//         labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
//         datasets: [{
//             label: '# of Votes',
//             data: [12, 19, 3, 5, 2, 3],
//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(255, 206, 86, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 'rgba(153, 102, 255, 0.2)',
//                 'rgba(255, 159, 64, 0.2)'
//             ],
//             borderColor: [
//                 'rgba(255, 99, 132, 1)',
//                 'rgba(54, 162, 235, 1)',
//                 'rgba(255, 206, 86, 1)',
//                 'rgba(75, 192, 192, 1)',
//                 'rgba(153, 102, 255, 1)',
//                 'rgba(255, 159, 64, 1)'
//             ],
//             borderWidth: 1
//         }]
//     },
//     options: {
//         scales: {
//             y: {
//                 beginAtZero: true
//             }
//         }
//     }
// });
