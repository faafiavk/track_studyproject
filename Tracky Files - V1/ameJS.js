// All poll data is in this constant (options, credits and colours)
const pollData = [
  {
    option: "Not Achieved",
    credits: 3,
    color: "#521a0c"
  },
  {
    option: "Achieved",
    credits: 9,
    color: "#98AD2D"
  },
  {
    option: "Merit",
    credits: 27,
    color: "#4E9B1C"
  },
  {
    option: "Excellence",
    credits: 23,
    color: "#184A0A"
  },
];

// Credits Breakdown Chart
const ctx = document.getElementById('chart').getContext('2d');
const pollChart = new Chart(ctx, {

  // Graph Type
    type: 'pie',
    data: {
        labels: pollData.map(pollOption => pollOption.option),
        datasets: [{
            data: pollData.map(pollOption => pollOption.credits),
            options: {
            legend: {
              display: false
            }
          },
            backgroundColor: pollData.map(pollOption => pollOption.color),
            borderWidth: 1
        }]
}
});
