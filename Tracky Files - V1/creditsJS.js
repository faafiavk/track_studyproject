// Data for the graphs
const pollData = [
  {
    option: "Physics",
    credits: 13,
    color: "#151929"
  },
  {
    option: "English",
    credits: 15,
    color: "#243B63"
  },
  {
    option: "Maths",
    credits: 12,
    color: "#447EBD"
  },
  {
    option: "Business",
    credits: 8,
    color: "#D2326C"
  },

  {
    option: "Economics",
    credits: 14,
    color: "#AC0113"
  },
];


// Credits Breakdown Chart
const ctx = document.getElementById('chart').getContext('2d');
const pollChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: pollData.map(pollOption => pollOption.option),
        datasets: [{

            // label: '# of Credits',
            data: pollData.map(pollOption => pollOption.credits),
            options: {
            legend: {
              display: false
            }
          },
            backgroundColor: pollData.map(pollOption => pollOption.color),
            borderWidth: 1
        }]
}
});
