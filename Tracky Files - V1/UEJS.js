// Data for the graphs
const pollData = [
  {
    option: "Numeracy",
    credits: 3,
    color: "#F53F15"
  },
  {
    option: "Reading",
    credits: 8,
    color: "#FC7A21"
  },
  {
    option: "Writing",
    credits: 5,
    color: "#FDD000"
  },
];

// UE Breakdown Chart
const ctx = document.getElementById('chart').getContext('2d');
const pollChart = new Chart(ctx, {
    type: 'polarArea',
    data: {
        labels: pollData.map(pollOption => pollOption.option),
        datasets: [{
            data: pollData.map(pollOption => pollOption.credits),
            options: {
            legend: {
              display: false
            },
            scales: {
              y: {
                min: 0,
                max: 10,
              }
            }
          },
            backgroundColor: pollData.map(pollOption => pollOption.color),
            borderWidth: 1
        }]
},

});
