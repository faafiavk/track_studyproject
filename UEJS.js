const pollData = [
  {
    option: "Literacy",
    votes: 11,
    color: "rgb(255, 99, 132)"
  },
  {
    option: "Reading",
    votes: 8,
    color: "rgb(54, 162, 235)"
  },
  {
    option: "Writing",
    votes: 11,
    color: "rgb(36, 36, 36)"
  },
];

// Credits Breakdown Chart
const ctx = document.getElementById('chart').getContext('2d');
const pollChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: pollData.map(pollOption => pollOption.option),
        datasets: [{

            // label: '# of Credits',
            data: pollData.map(pollOption => pollOption.votes),
            options: {
            legend: {
              display: false
            }
          },
            backgroundColor: pollData.map(pollOption => pollOption.color),
            borderWidth: 1
        }]
}
});
