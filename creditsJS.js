const pollData = [
  {
    option: "Not Achieved",
    votes: 3,
    color: "rgb(255, 99, 132)"
  },
  {
    option: "Achieved",
    votes: 8,
    color: "rgb(54, 162, 235)"
  },
  {
    option: "Merit",
    votes: 10,
    color: "rgb(36, 36, 36)"
  },
  {
    option: "Excellence",
    votes: 7,
    color: "rgb(255, 159, 64)"
  },
];

// Credits Breakdown Chart
const ctx = document.getElementById('chart').getContext('2d');
const pollChart = new Chart(ctx, {
    type: 'polarArea',
    data: {
        labels: pollData.map(pollOption => pollOption.option),
        datasets: [{

            // label: '# of Credits',
            data: pollData.map(pollOption => pollOption.votes),
            options: {
            legend: {
              display: false
            },
            scales: {
              y: {
                min: 0,
                max: 10,
              }
            }
          },
            backgroundColor: pollData.map(pollOption => pollOption.color),
            borderWidth: 1
        }]
},

});
